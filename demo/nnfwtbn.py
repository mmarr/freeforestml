import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


# +
class Selector:
    """
    Base class to represent event selection. This class selects all events.
    Sub classes should overwrite the select() method.
    """
    def __init__(self, func=None):
        if func is None:
            func =  lambda df: np.ones(len(df), dtype='bool')
        self.func = func
        
    def __call__(self, dataframe):
        """
        Returns an index array with all the selected events.
        """
        return self.func(dataframe)
    
    def __and__(self, other):
        if callable(other):
            return Selector(lambda df: self(df) & other(df))
        else:
            return Selector(lambda df: self(df) & other)
    def __rand__(self, other):
        return self & other
    def __or__(self, other):
        return Selector(lambda df: self(df) | other)
    def __ror__(self, other):
        return self | other
    def __xor__(self, other):
        return Selector(lambda df: self(df) ^ other)
    def __rxor__(self, other):
        return self ^ other
        
        
    
def selin(start, stop):
    """
    Select process in [start, stop] including the borders.
    """
    return lambda df: (start <= df.fpid) & (df.fpid <= stop)
        
class Process:
    """
    Base class to represent a physics process shown in the legent.
    """
    index = 0  # Used for color palette
    palette = sns.color_palette("hls", 8)
    
    def __init__(self, name, selector, style, color):
        self.name = name
        self.selector = selector
        self.style = style
        self.color = color
        
    def color(self):
        return self.color
    
def access(var):
    if isinstance(var, str):
        return lambda df: df[var]
    else:
        return var
        
    
def histo_dist(dataframe, variable, processes, bins, range, selector=Selector(),
            x_unit="GeV", x_label=None, weight_var='weight', *args, **kwds):
    """
    Visualizes the event counts as a stacked histogram.
    
    Parameters:
        dataframe - A pandas dataframe with all the events to process
        variable  - A string naming the column to plot or a callable which
                    computes the variable to plot
        selector  - A selector object to apply cuts
        processes - A list of process objects
    """
    stacked = [p for p in processes if p.style == 'stacked']
    if stacked:
        plt.hist([access(variable)(dataframe[p.selector(dataframe) & selector(dataframe)]) for p in stacked],
                 *args, bins=bins, range=range,
                 color=[p.color for p in stacked],
                 label=[p.name for p in stacked],
                 weights= [access(weight_var)(dataframe[p.selector(dataframe) & selector(dataframe)]) for p in stacked],
                 stacked=True, **kwds)
    
    line = [p for p in processes if p.style == 'line']
    if line:
        plt.hist([access(variable)(dataframe[p.selector(dataframe) & selector(dataframe)]) for p in line],
                 *args, bins=bins, range=range,
                 color=[p.color for p in line],
                 histtype='step',
                 weights=[access(weight_var)(dataframe[p.selector(dataframe) & selector(dataframe)]) for p in line],
                 label=[p.name for p in line],
                 **kwds)
    
    data = [p for p in processes if p.style == 'data']
    if data:
        for p in data:
            histo, bin_edges = np.histogram(access(variable)(dataframe[p.selector(dataframe) & selector(dataframe)]),
                                       weights=access(weight_var)(dataframe[p.selector(dataframe) & selector(dataframe)]),
                                       bins=bins, range=range)

            bin_widths = bin_edges[1:] - bin_edges[:-1]
            bin_centers = (bin_edges[1:] + bin_edges[:-1]) / 2

            plt.errorbar(bin_centers, histo, np.sqrt(histo), bin_widths / 2,
                         color=p.color, label=p.name, fmt='o', markersize=4, **kwds)

    plt.legend()
    plt.xlim(range)
    plt.ylabel("Events / %g %s" % ((range[1] - range[0]) / bins, x_unit))
    if x_label is not None:
        if x_unit is None:
            plt.xlabel(x_label)
        else:
            plt.xlabel("%s in %s" % (x_label, x_unit))
        


# -
df = pd.read_hdf("mva.h5")


class VBFSelector(Selector):
    def __init__(self):
        self.func = self.internal
        
    @staticmethod
    def internal(df):
        sel = df.dijet_p4__M  > 400 
        sel &= df.jet_1_p4__Pt >= 30
        sel &= df.is_dijet_centrality == 1
        sel &= df.jet_0_p4__Eta * df.jet_1_p4__Eta < 0
        sel &= (df.jet_0_p4__Eta - df.jet_1_p4__Eta).abs() > 3
        return sel


# +
sig = sns.color_palette()
bkg = sns.color_palette("Blues")

processes = []
processes.append(Process("Fake", selin(-199, -100), 'stacked', bkg[len(processes)]))
processes.append(Process("Top", selin(-499, -400), 'stacked', bkg[len(processes)]))
processes.append(Process("Diboson", selin(-399, -300), 'stacked', bkg[len(processes)]))
processes.append(Process("$Z\\rightarrow\\tau\\tau$", selin(-699, -600), 'stacked', bkg[len(processes)]))
processes.append(Process("$Z\\rightarrow\\ell\\ell$", selin(-599, -500), 'stacked', "#99dddd"))
processes.append(Process("Signal", selin(1, 999), 'stacked', sig[1]))
processes.append(Process("Data", selin(0, 0), 'data', 'black'))
# -

histo_dist(df,
           "ditau_p4__Pt",
           processes,
           range=(25, 225),
           bins=20,
           selector=1 ^ VBFSelector(),
           x_label="$m_{\mathrm{MMC}}$")

histo_dist(df,
           "fpid",
           processes,
           range=(-999, 999),
           bins=100,
           x_label="$m_{\mathrm{MMC}}$")
plt.yscale('log')
v = VBFSelector()

v.func

histo_dist(df,
           "weight",
           [processes[4]],
           range=(-1, 1),
           bins=24,
           selector=VBFSelector(),
           x_label="Event weight",
           x_unit=None)

df[VBFSelector() & selin(-599, -500)].weight.sum()



